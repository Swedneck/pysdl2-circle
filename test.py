#!/bin/env python3
import sys
import sdl2
import sdl2.ext
import time

WHITE = sdl2.ext.Color(255, 255, 255)
BLACK = sdl2.ext.Color(0, 0, 0)


def draw_circle(renderer, xc, yc, r, color=None):
    def _point(x, y):
        renderer.draw_point((x, y), color)

    def _draw(xc, yc, x, y):
        _point(xc+x, yc+y)
        _point(xc-x, yc+y)
        _point(xc+x, yc-y)
        _point(xc-x, yc-y)
        _point(xc+y, yc+x)
        _point(xc-y, yc+x)
        _point(xc+y, yc-x)
        _point(xc-y, yc-x)

    d = 3 - (2*r)
    x = 0
    y = r

    _draw(xc, yc, x, y)
    while x <= y:
        x += 1
        if d < 0:
            d = d + (4*x) + 6
        else:
            d = d + 4*(x-y) + 10
            y -= 1
        _draw(xc, yc, x, y)


def run():
    sdl2.ext.init()
    window = sdl2.ext.Window("Hello World!", size=(900, 600))
    window.show()
    renderer = sdl2.ext.Renderer(window)

    # Initialize window
    renderer.clear(BLACK)
    renderer.present()

    radius = int(window.size[0] / window.size[1] * (min(window.size)//10))

    # Draw the circle
    draw_circle(renderer, (window.size[0]//2), (window.size[1]//2), radius,
                WHITE)
    renderer.present()

    # Main loop
    running = True
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
                break
    return 0


if __name__ == '__main__':
    sys.exit(run())
